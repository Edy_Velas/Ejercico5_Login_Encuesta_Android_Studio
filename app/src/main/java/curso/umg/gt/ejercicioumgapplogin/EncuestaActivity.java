package curso.umg.gt.ejercicioumgapplogin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;


import java.util.ArrayList;
import java.util.List;

public class EncuestaActivity extends AppCompatActivity {

    private EditText edt1,edt2,edt3;
    private ListView lv1;
    private List<String> listado;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encuesta);
        listado=new ArrayList<>();


        edt1= (EditText) findViewById(R.id.et1);
        edt2=(EditText)findViewById(R.id.et2);
        edt3=(EditText)findViewById(R.id.et3);
        lv1= (ListView) findViewById(R.id.lv1 );


        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listado);
        lv1.setAdapter(adapter);

    }


    public void add(View view){
        String value=edt1.getText().toString();
        String value2=edt2.getText().toString();
        String value3=edt3.getText().toString();

        listado.add(value);
        listado.add(value2);
        listado.add(value3);
        adapter.notifyDataSetChanged();

        edt1.setText("");
        edt2.setText("");
        edt3.setText("");
    }
}